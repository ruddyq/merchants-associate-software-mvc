/**
 * Materia: Software Architecture
 * UAGRM - FICCT
 * @author: Ruddy Bryan Quispe Mamani 
 * @version: 1.0.0
 * @since: 09-10-2021
 */

/**
 * Libraries Imports
 */
import { serverHTTP, Application } from './options';
import morgan from 'morgan';
import path from 'path';
import expressHandlebars from 'express-handlebars';
import methodOverride from 'method-override';
// import { Connection } from './database/connection'; // connection to database

/**
 * Views Imports (routes)
 */
import { SectorController } from './controller/sector.controller';
import { AporteController } from './controller/aporte.controller';
import { PuestoController } from './controller/puesto.controller';
import { SocioController } from './controller/socio.controller';
import { AsistenciaController } from './controller/asistencia.controller';
import { PagoAporteController } from './controller/pago_aporte.controller';

/**
 * Main Class App
 */
export class App {

    /**
     * Attributes
     */
    private app: Application;

    /**
     * Initialize http server
     * @param port port to initialize the HTTP server
     */
    constructor(port?: number | string) {
        this.app = serverHTTP();   // starting the web server
        this.app.set('PORT', process.env.PORT || port || 3000); // saving the port in a global variable
        this.setting();
        this.middlewares();
        this.routes();
    }

    /**
     * Initialize the HTTP server settings (settings)
     * configurations:
     * - address of views (hbs and presentation classes)
     * - address of view partitions (hbs)
     * - enabling the sending of data through the url
     * - enable methodOverride for HTTP.PUT and HTTP.DELETE requests
     */
    private setting(): void {
        this.app.set("views", path.join(__dirname, "view"));
        // Initialize handlebars engine
        let hbs = expressHandlebars.create({
            extname: '.hbs',    // files extensions
            partialsDir: path.join(this.app.get('views'), 'partials'),  // partitions hbs
            defaultLayout: 'main',  // main file
            layoutsDir: path.join(this.app.get('views'), 'layout'),
            helpers: {
                foo: function (a: Number, b: Number, opts: any) {
                    return (a == b) ? opts.fn(this) : opts.inverse(this);;
                }
            }
        });
        this.app.engine('.hbs', hbs.engine);
        this.app.set('view engine', '.hbs');                             // using handlebars
        this.app.use(serverHTTP.urlencoded({ extended: true }));
        this.app.use(methodOverride('_method'));                      // you can to send html methods as put, delete

    }

    /**
     * Middleware is run before proceeding with an HTTP request
     * - Morgan enable (description of HTTP requests in the console)
     * - enabling of information input by JSON
     */
    private middlewares(): void {
        this.app.use(morgan('dev'));
        this.app.use(serverHTTP.json());                                          // motor of functionality messages
    }

    /**
     * Function for the creation of the presentation layer instances
     * Use Cases to be instantiated:
     * - Sector
     */
    private routes(): void {
        this.app.get('/', async (req, res) => {
            res.render('dashboard');
        });
        // Use Case Basic
        let sectorController: SectorController = new SectorController();
        this.app.use('/sector_manage', sectorController.router);
        // Use Case Basic
        let aporteController: AporteController = new AporteController();
        this.app.use('/aporte_manage', aporteController.router);
        // Use Case Complicated
        let puestoController: PuestoController = new PuestoController();
        this.app.use('/puesto_manage', puestoController.router);
        // Use Case Complicated
        let socioController: SocioController = new SocioController();
        this.app.use('/socio_manage', socioController.router);
        // Transactional Use Case
        let asistenciaController: AsistenciaController = new AsistenciaController();
        this.app.use('/asistencia_manage', asistenciaController.router);
        // Transactional Use Case
        let pagoAporteController: PagoAporteController = new PagoAporteController();
        this.app.use('/pago_aporte_manage', pagoAporteController.router);
    }

    /**
     * Method for the execution of the HTTP server through the previously saved port
     */
    public async listen(): Promise<void> {
        await this.app.listen(this.app.get('PORT'));
        console.log("\x1b[46m", "\x1b[30m", `Server on port ${this.app.get('PORT')}`, "\x1b[0m");
    }
}