/**
 * Materia: Arquitectura de Software
 * UAGRM - FICCT
 * @author: Ruddy Bryan Quispe Mamani 
 * @version: 0.0.1
 * @since: 29-10-2021
 */

import { AsistenciaModel } from '../model/asistencia.model';
import { AsistenciaSocioModel } from '../model/asistencia_socio.model';
import { SocioModel } from '../model/socio.model';
import { Request, Response, Router } from '../options';
import { AsistenciaView } from '../view/asistencia_manage/asistencia.view';

export class AsistenciaController {
    /**
     * Attributes
     */
    public router: Router;
    private asistenciaModel: AsistenciaModel;
    private socioModel: SocioModel;
    private asistenciaView: AsistenciaView;

    /**
     * Constructor
     */
    constructor() {
        this.router = Router();
        this.createRoutes();
        this.asistenciaModel = new AsistenciaModel();
        this.socioModel = new SocioModel();
        this.asistenciaView = new AsistenciaView();
    }

    /**
     * return list associate with views and options post
     * @param request : request de HTTP
     * @param response : response de HTTP
     */
    public async getViewAsistenciaManage(request: Request, response: Response): Promise<void> {
        let listAssociates: Array<{ ci: number, nombre: string, telefono: string, direccion: string, fecha_afiliacion: string, estado: boolean, nro_puesto: number }>
            = await this.socioModel.findAll();
        await this.asistenciaView.getAsistenciaManageView(response, listAssociates);
    }

    /**
     * create new attendance
     * @param request : request HTTP
     * @param response : response HTTP
     */
    public async createAsistencia(request: Request, response: Response): Promise<void> {
        console.log(request.body);
        let { descripcion, fecha_realizado, socio } = request.body;
        this.asistenciaModel.setDescripcion(descripcion);
        this.asistenciaModel.setFechaRealizado(fecha_realizado);
        this.asistenciaModel.setListaSocios(socio, -1);
        let created: boolean = await this.asistenciaModel.create();
        if (created) {
            await this.asistenciaView.redirectAsistenciaManageView(response);
        } else {
            await this.asistenciaView.getMessageErrorView(response, "No se pudo guardar la nueva asistencia");
        }
    }

    /**
     * get a contribution specified
     * @param request : request HTTP
     * @param response : response HTTP
     */
    public async getViewEditAsistencia(request: Request, response: Response): Promise<void> {
        let { cod_asistencia } = request.params;
        this.asistenciaModel.setCodigo(parseInt(cod_asistencia));
        let listSocio: Array<{ ci: number, nombre: string, telefono: string, direccion: string, fecha_afiliacion: string, estado: boolean, nro_puesto: number, verify_asistencia: boolean }>
            = await this.asistenciaModel.findSociosAsistencia();
        await this.asistenciaView.getEditAsistenciaView(response, parseInt(cod_asistencia), listSocio);
    }

    /**
     * get a associate specified
     * @param request : request HTTP
     * @param response : response HTTP
     */
    public async getListAsistenciaSocio(request: Request, response: Response): Promise<void> {
        let { cod_asistencia } = request.params;
        this.asistenciaModel.setCodigo(parseInt(cod_asistencia));
        let listAttendance = await this.asistenciaModel.findAsistenciaList();
        await this.asistenciaView.getListAsistencia(response, listAttendance);
    }

    /**
     * update a associate
     * @param request : request HTTP
     * @param response : response HTTP
     */
    public async updateAsistencia(request: Request, response: Response): Promise<void> {
        let { cod_asistencia } = request.params;
        let { descripcion, fecha_realizado, socio } = request.body;
        this.asistenciaModel.setDescripcion(descripcion);
        this.asistenciaModel.setFechaRealizado(fecha_realizado);
        this.asistenciaModel.setListaSocios(socio, parseInt(cod_asistencia));
        let updated: boolean = await this.asistenciaModel.update();
        if (updated) {
            await this.asistenciaView.redirectAsistenciaManageView(response);
        } else {
            await this.asistenciaView.getMessageErrorView(response, "No se pudo modificar la asistencia Nro." + cod_asistencia + ", intente nuevamente por favor");
        }
    }

    /**
     * remove one associate
     * @param request : request HTTP
     * @param response : response HTTP
     */
    public async deleteAssociate(request: Request, response: Response): Promise<void> {
        let { cod_asistencia } = request.params;
        this.asistenciaModel.setCodigo(parseInt(cod_asistencia));
        let deleted: boolean = await this.asistenciaModel.delete();
        if (deleted) {
            let listAssociates: Array<{ ci: number, nombre: string, telefono: string, direccion: string, fecha_afiliacion: string, estado: boolean, nro_puesto: number }>
                = await this.socioModel.findAll();
            await this.asistenciaView.getAsistenciaManageView(response, listAssociates);
        } else {
            this.asistenciaView.getMessageErrorView(response, "No se pudo eliminar la asistencia Nro: " + cod_asistencia);
        }
    }

    /**
     * metodo privado para cargar las rutas que disponen en los metodos HTTP
     */
    private createRoutes(): void {
        this.router.route('/').get(async (req: Request, res: Response) => this.getViewAsistenciaManage(req, res));
        this.router.route('/').post(async (req: Request, res: Response) => this.createAsistencia(req, res));
        this.router.route('/:cod_asistencia').get(async (req: Request, res: Response) => this.getViewEditAsistencia(req, res));
        this.router.route('/detail/:cod_asistencia').get(async (req: Request, res: Response) => this.getListAsistenciaSocio(req, res));
        this.router.route('/update/:cod_asistencia').put(async (req: Request, res: Response) => this.updateAsistencia(req, res));
        this.router.route('/delete/:cod_asistencia').delete(async (req: Request, res: Response) => this.deleteAssociate(req, res));
    }
}