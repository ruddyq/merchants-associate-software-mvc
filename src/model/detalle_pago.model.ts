/**
 * Materia: Software Architecture
 * UAGRM - FICCT
 * @author: Ruddy Bryan Quispe Mamani 
 * @version: 1.0.0
 * @since: 29-10-2021
 */

/**
 * Libraries Import
 */
import { QueryResult } from "pg";
import { Connection } from "../database/connection";

/**
 * @class Attendance Associate by Model Package contained MVC architecture
 */
export class DetallePagoModel {

    /**
     * Attributes
     */
    private nroPagoAporte: number;
    private codAporte: string;
    private monto: number;
    private connectionSQL: Connection;

    /**
     * @constructor
     */
    constructor() {
        this.nroPagoAporte = -1;
        this.codAporte = "";
        this.monto = 0;
        this.connectionSQL = Connection.getInstance();
    }

    public setNroPagoAporte = (nroPagoAporte: number): void => { this.nroPagoAporte = nroPagoAporte; }
    public setCodAporte = (codAporte: string): void => { this.codAporte = codAporte; }
    public setMonto = (monto: number): void => { this.monto = monto; }
    public getNroPagoAporte = (): number => this.nroPagoAporte;
    public getCodAporte = (): string => this.codAporte;
    public getMonto = (): number => this.monto;

    /**
     * create a new Attendance-Associate register
     * @returns true if saved successfully
     */
    public create = async (): Promise<boolean> => {
        try {
            await this.connectionSQL.executeSQL(
                `insert into detalle_pago_aporte(nro_pago,monto,cod_aporte) values (${this.nroPagoAporte},${this.monto},'${this.codAporte}');`
            );
            return true;
        } catch (error) {
            console.log("\x1b[41m", "\x1b[37m", `Error into DetallePagoModel -> create()`, "\x1b[0m");
            return false;
        }
    }

    /**
     * remove an Attendance of nroPagoAporte assigned DetallePagoModel.nroPagoAporte
     * @returns true if Attendance was removed, return false if has a trouble
     */
    public delete = async (): Promise<boolean> => {
        try {
            await this.connectionSQL.executeSQL(
                `delete from detalle_pago_aporte where nro_pago=${this.nroPagoAporte} and cod_aporte='${this.codAporte}';`
            );
            return true;
        } catch (error) {
            console.log("\x1b[41m", "\x1b[37m", `Error into DetallePagoModel -> delete()`, "\x1b[0m");
            return false;
        }
    }

    public deleteAll = async (): Promise<boolean> => {
        try {
            await this.connectionSQL.executeSQL(
                `delete from detalle_pago_aporte where nro_pago=${this.nroPagoAporte}`
            );
            return true;
        } catch (error) {
            console.log("\x1b[41m", "\x1b[37m", `Error into DetallePagoModel -> deleteAll()`, "\x1b[0m");
            return false;
        }
    }
}