/**
 * Materia: Arquitectura de Software
 * UAGRM - FICCT
 * @author: Ruddy Bryan Quispe Mamani 
 * @version: 0.0.1
 * @since: 22-10-2021
 */

import { PuestoModel } from '../../model/puesto.model';
import { Response } from '../../options';

export class PuestoView {

    /**
     * Attributes
     */
    private nroPuesto: number;
    private estado: boolean;            // true: habilitado * false: inhabilitado
    private idSector: number;
    private puestoModel: PuestoModel;

    /**
     * Constructor
     */
    constructor() {
        this.nroPuesto = -1;
        this.estado = false;
        this.idSector = -1;
        this.puestoModel = new PuestoModel();
    }

    public setNroPuesto = (nroPuesto: number): void => { this.nroPuesto = nroPuesto; }
    public setEstado = (estado: boolean): void => { this.estado = estado; }
    public setIdSector = (idSector: number): void => { this.idSector = idSector; }

    public getNroPuesto = (): number => this.nroPuesto;
    public getEstado = (): boolean => this.estado;
    public getIdSector = (): number => this.idSector;

    /**
     * update view Shop Manage
     * @param response : response HTTP
     */
    public async getShopManageView(response: Response, sectorList: Array<{ id: number, descripcion: string }>): Promise<void> {
        let shopList = await this.puestoModel.findAll();
        response.render('puesto_manage/puesto_manage', {
            shop_list: shopList,
            sector_list: sectorList
        });
    }

    /**
     * render view shop manage
     * @param response response http
     */
    public async redirectShopManageView(response: Response): Promise<void> {
        response.redirect('/puesto_manage');
    }

    /**
     * get edit shop edit
     * @param response : request HTTP
     * @param nroPuesto : shop No selected
     */
    public async getEditShopView(response: Response, nroPuesto: number, sectorList: Array<{ id: number, descripcion: string }>): Promise<void> {
        this.puestoModel.setNroPuesto(nroPuesto);
        let shopData: { nro_puesto: number, estado: boolean, id_sector: number, descripcion: string } | undefined
            = await this.puestoModel.findById();
        if (shopData) {
            // if shop exists
            response.status(200).render('puesto_manage/puesto_edit', {
                shop_data: shopData,
                sector_list: sectorList
            });
        } else {
            // si no existe categoria con el idCategoria (undefined)
            this.getMessageErrorView(response, "No se encontro los datos del puesto seleccionado");
        }
    }

    /**
     * Render view error
     * @param response : HTTP response
     * @param message : message error to send
     */
    public async getMessageErrorView(response: Response, message: string): Promise<void> {
        response.render('error', { message_error: message });
    }
}