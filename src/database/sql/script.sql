-- CREATE ROLE
CREATE ROLE merchants_association_october_4
    WITH
    LOGIN
    INHERIT
    REPLICATION
    CONNECTION LIMIT -1
    PASSWORD 'b6200836-fea7-4a86-9d33-107d08655f33';

-- CREATE DATABASE
CREATE DATABASE merchants_association_october_4 
    OWNER merchants_association_october_4;

------------------------------------------------------
-- CREATION SQL TABLES
------------------------------------------------------
create table sector (
	id serial primary key,
	descripcion varchar(25) not null
);
select id, descripcion from sector where id=2;
insert into sector(descripcion) values ('Abarrotes');
insert into sector(descripcion) values ('Ferreteria');
insert into sector(descripcion) values ('Comidas y Snack');
insert into sector(descripcion) values ('Ropa');
insert into sector(descripcion) values ('Plasticos');
update sector set descripcion='frutas' where id=1;

create table puesto (
	nro_puesto integer not null primary key,
	estado boolean not null,
	id_sector integer not null,
	foreign key (id_sector) references sector(id)
	on update cascade
	on delete cascade
);
select p.nro_puesto, p.stado, p.id_sector, s.descripcion 
	from puesto p, sector s 
	where p.id_sector=s.id and p.nro_puesto=22;
insert into puesto(nro_puesto, estado, id_sector) values (22, true, 1);
update puesto set estado=false, id_sector=1 where nro_puesto=22;
update puesto set estado=not estado where nro_puesto=22;
delete from puesto where nro_puesto=12;

create table socio (
	ci integer not null primary key,
	nombre varchar(150) not null,
	telefono varchar(9) not null,
	direccion varchar(255) not null,
	fecha_afiliacion date not null,
	estado boolean not null,
	nro_puesto integer null,
	foreign key (nro_puesto) references puesto(nro_puesto)
	on update no action
	on delete no action
);

create table aporte (
	codigo varchar(7) not null primary key,
	descripcion varchar(255) not null,
	fecha_creacion date not null,
	monto decimal(8,2) not null
);

create table pago_aporte (
	nro_pago serial not null primary key,
	total_monto decimal(8,2) not null,
	fecha_pago date not null,
	ci_socio integer not null,
	foreign key (ci_socio) references socio(ci)
	on update cascade
	on delete cascade
);

create table detalle_pago_aporte (
	nro_pago integer not null,
	cod_aporte varchar(7) not null,
	monto decimal(8,2) not null,
	primary key (nro_pago, cod_aporte),
	foreign key (nro_pago) references pago_aporte(nro_pago)
	on update cascade
	on delete cascade,
	foreign key (cod_aporte) references aporte(codigo)
	on update cascade
	on delete cascade
);

create table asistencia (
	codigo serial primary key,
	descripcion varchar(50) not null,
	fecha_realizado date not null
);

create table asistencia_socio (
	ci_socio integer not null,
	cod_asistencia integer not null,
	primary key (ci_socio,cod_asistencia),
	foreign key (cod_asistencia) references asistencia(codigo)
	on update cascade
	on delete cascade,
	foreign key (ci_socio) references socio(ci)
	on update cascade
	on delete cascade
);

create or replace function verify_asistencia(cod_asistencia_v int, ci_socio_v int) returns boolean as
$BODY$
declare exists_in_asistencia int = (select count(*) from asistencia_socio where cod_asistencia=cod_asistencia_v and ci_socio=ci_socio_v);
begin
	if (exists_in_asistencia=0) then
		return false;
	else
		return true;
	end if;
end;
$BODY$ language plpgsql;