/**
 * Materia: Arquitectura de Software
 * UAGRM - FICCT
 * @author: Ruddy Bryan Quispe Mamani 
 * @version: 0.0.1
 * @since: 10-10-2021
 */

import { SectorModel } from '../model/sector.model';
import { Request, Response, Router } from '../options';
import { SectorView } from '../view/sector_manage/sector.view';

export class SectorController {
    /**
     * Attributes
     */
    public router: Router;
    private sectorModel: SectorModel;
    private sectorView: SectorView;

    /**
     * Constructor
     */
    constructor() {
        this.router = Router();
        this.createRoutes();
        this.sectorModel = new SectorModel();
        this.sectorView = new SectorView();
    }

    /**
     * return list sectors with views
     * @param request : request de HTTP
     * @param response : response de HTTP
     */
    public async getViewSectorManage(request: Request, response: Response): Promise<void> {
        await this.sectorView.getSectorManageView(response);
    }

    /**
     * create new sector
     * @param request : request HTTP
     * @param response : response HTTP
     */
    public async createSector(request: Request, response: Response): Promise<void> {
        let { description } = request.body;
        this.sectorModel.setDescripcion(description);
        let created = await this.sectorModel.create();
        if (created) {
            await this.sectorView.redirectSectorManageView(response);
        } else {
            this.sectorView.getMessageErrorView(response, "No se pudo guardar el nuevo sector");
        }
    }

    /**
     * get a sector specified
     * @param request : request HTTP
     * @param response : response HTTP
     */
    public async getViewEditSector(request: Request, response: Response): Promise<void> {
        let { id_sector } = request.params;
        await this.sectorView.getEditSectorView(response, Number(id_sector));
    }

    /**
     * update a sector
     * @param request : request HTTP
     * @param response : response HTTP
     */
    public async updateSector(request: Request, response: Response): Promise<void> {
        let { id_sector } = request.params;
        let { description } = request.body;
        this.sectorModel.setId(parseInt(id_sector));
        this.sectorModel.setDescripcion(description);
        let updated = await this.sectorModel.update();
        if (updated) {
            // modificacion de libro correctamente
            this.sectorView.redirectSectorManageView(response);
        } else {
            // hubo errores en modificacion de categoria
            this.sectorView.getMessageErrorView(response, "No se pudo actualizar el sector");
        }
    }

    /**
     * remove one sector
     * @param request : request HTTP
     * @param response : response HTTP
     */
    public async deleteSector(request: Request, response: Response): Promise<void> {
        let { id_sector } = request.params;
        this.sectorModel.setId(parseInt(id_sector));
        let deleted = await this.sectorModel.delete();
        if (deleted) {
            this.sectorView.getSectorManageView(response);
        } else {
            this.sectorView.getMessageErrorView(response, "No se pudo eliminar el sector");
        }
    }

    /**
     * metodo privado para cargar las rutas que disponen en los metodos HTTP
     */
    private createRoutes(): void {
        this.router.route('/').get(async (req: Request, res: Response) => this.getViewSectorManage(req, res));
        this.router.route('/').post(async (req: Request, res: Response) => this.createSector(req, res));
        this.router.route('/:id_sector').get(async (req: Request, res: Response) => this.getViewEditSector(req, res));
        this.router.route('/update/:id_sector').put(async (req: Request, res: Response) => this.updateSector(req, res));
        this.router.route('/delete/:id_sector').delete(async (req: Request, res: Response) => this.deleteSector(req, res));
    }
}