/**
 * Materia: Software Architecture
 * UAGRM - FICCT
 * @author: Ruddy Bryan Quispe Mamani 
 * @version: 1.0.0
 * @since: 29-10-2021
 */

/**
 * Libraries Import
 */
import { QueryResult } from "pg";
import { Connection } from "../database/connection";
import { DetallePagoModel } from "./detalle_pago.model";

/**
 * @class PagoAporte by Model Package contained MVC architecture
 */
export class PagoAporteModel {

    /**
     * Attributes
     */
    private nro: number;
    private fechaPago: Date;
    private montoTotal: number;
    private ciSocio: number;
    private listaDetallePago: Array<DetallePagoModel>;
    private connectionSQL: Connection;

    /**
     * @constructor
     */
    constructor() {
        this.nro = -1;
        this.fechaPago = new Date();
        this.montoTotal = -1;
        this.ciSocio = -1;
        this.connectionSQL = Connection.getInstance();
        this.listaDetallePago = new Array<DetallePagoModel>();
    }

    public setNro = (nro: number): void => { this.nro = nro; }
    public setFechaPago = (fechaPago: Date): void => { this.fechaPago = fechaPago; }
    public setMontoTotal = (montoTotal: number): void => { this.montoTotal = montoTotal; }
    public setListaDetallePago = (listaDetalle: Array<{ cod_aport: string, monto: string }>, nroPago: number): void => {
        this.listaDetallePago = new Array<DetallePagoModel>();
        for (let index = 0; index < listaDetalle.length; index++) {
            let detallePago: { cod_aport: string, monto: string } = listaDetalle[index];
            this.listaDetallePago.push(new DetallePagoModel());
            this.listaDetallePago[index].setCodAporte(detallePago.cod_aport);
            this.listaDetallePago[index].setNroPagoAporte(nroPago);
            this.listaDetallePago[index].setMonto(parseInt(detallePago.monto));
        }
    }
    public setCiSocio = (ciSocio: number): void => { this.ciSocio = ciSocio; }

    public getNro = (): number => this.nro;
    public getFechaPago = (): Date => this.fechaPago;
    public getMontoTotal = (): number => this.montoTotal;
    public getListaDetallePago = (): Array<DetallePagoModel> => this.listaDetallePago;
    public getCiSocio = (): number => this.ciSocio;

    /**
     * create a new PagoAporte
     * @returns true if saved successfully
     */
    public create = async (): Promise<boolean> => {
        try {
            let result = await this.connectionSQL.executeSQL(
                `insert into pago_aporte(total_monto, fecha_pago, ci_socio) values (${this.montoTotal},'${this.fechaPago}',${this.ciSocio}) returning nro_pago;`
            );
            if (!result) {
                return false;
            }
            for await (let detallePago of this.listaDetallePago) {
                detallePago.setNroPagoAporte(result.rows[0].nro_pago);
                let createdDetailPagoAporte: boolean = await detallePago.create();
                if (!createdDetailPagoAporte) {
                    console.error("\x1b[41m", "\x1b[37m", `Error into PagoAporteModel -> create() -> detailPago.create()`, "\x1b[0m");
                    return false;
                }
            }
            return true;
        } catch (error) {
            console.error("\x1b[41m", "\x1b[37m", `Error into PagoAporteModel -> create()`, "\x1b[0m");
            return false;
        }
    }

    /**
     * find by nro PagoAporte saved
     * @returns object PagoAporte or undefined if have an error or not find
     */
    public findById = async (): Promise<{ nro: number, montoTotal: string, fecha_realizado: string } | undefined> => {
        try {
            let PagoAporteObtained: QueryResult =
                await this.connectionSQL.executeSQL(
                    `select pa.nro_pago, pa.total_monto, to_char(pa.fecha_pago ::DATE,'yyyy-mm-dd') as fecha_pago, pa.ci_socio, s.nombre 
                    from pago_aporte pa, socio s 
                    where pa.ci_socio=s.ci and pa.nro_pago=${this.nro};`
                );
            return PagoAporteObtained.rows[0];
        } catch (error) {
            console.error("\x1b[41m", "\x1b[37m", `Error into PagoAporteModel -> findById()`, "\x1b[0m");
            return undefined;
        }
    }

    /**
     * find by nroPagoAporte Attendance saved
     * @returns object Attendance or undefined if have an error or not find
     */
    public findDetailById = async (): Promise<Array<{ codigo: string, descripcion: string, monto: number, nro_pago: number }>
        | undefined> => {
        try {
            let AttendanceObtained: QueryResult = await this.connectionSQL.executeSQL(
                `select a.codigo, a.descripcion, dpa.monto, dpa.nro_pago 
                    from detalle_pago_aporte dpa, aporte a 
                    where dpa.cod_aporte=a.codigo and dpa.nro_pago=${this.nro};`
            );
            return AttendanceObtained.rows;
        } catch (error) {
            console.log("\x1b[41m", "\x1b[37m", `Error into DetallePagoModel -> findById()`, "\x1b[0m");
            return undefined;
        }
    }

    /**
     * update fechaMake of a PagoAporte with nro assigned
     * @returns true if updated else return false
     */
    public update = async (): Promise<boolean> => {
        try {
            await this.connectionSQL.executeSQL(
                `update pago_aporte set total_monto=${this.montoTotal}, fecha_pago ='${this.fechaPago}', ci_socio=${this.ciSocio} where nro_pago=${this.nro}`
            );
            let clean = await this.listaDetallePago[0].deleteAll();
            if (clean) {
                for await (let detailPagoAporte of this.listaDetallePago) {
                    let createdAssociatePagoAporte = await detailPagoAporte.create();
                    if (!createdAssociatePagoAporte) {
                        console.error("\x1b[41m", "\x1b[37m", `Error into PagoAporteModel -> delete() -> associate.create()`, "\x1b[0m");
                        return false;
                    }
                }
            } else {
                return false;
            }
            return true;
        } catch (error) {
            console.error("\x1b[41m", "\x1b[37m", `Error into PagoAporteModel -> update()`, "\x1b[0m");
            return false;
        }
    }

    /**
     * remove an PagoAporte of nro assigned PagoAporteModel.nro
     * @returns true if PagoAporte was removed, return false if has a trouble
     */
    public delete = async (): Promise<boolean> => {
        try {
            await this.connectionSQL.executeSQL(
                `delete from detalle_pago_aporte where nro_pago=${this.nro};`
            );
            await this.connectionSQL.executeSQL(
                `delete from pago_aporte where nro_pago=${this.nro};`
            );
            return true;
        } catch (error) {
            console.error("\x1b[41m", "\x1b[37m", `Error into PagoAporteModel -> delete()`, "\x1b[0m");
            console.log(error);
            return false;
        }
    }

    /**
     * get list PagoAporte saved
     * @returns Array data PagoAporte
     */
    public findAll = async (): Promise<Array<{ nro_pago: number, total_monto: number, fecha_pago: string, ci_socio: number, nombre: string }>> => {
        try {
            let PagoAporteList = await this.connectionSQL.executeSQL(
                `select pa.nro_pago, pa.total_monto, pa.fecha_pago, pa.ci_socio, s.nombre 
                from pago_aporte pa, socio s 
                where pa.ci_socio=s.ci
                order by to_char(pa.fecha_pago ::DATE,'mm-dd-yyyy') desc;`
            );
            return PagoAporteList.rows;
        } catch (error) {
            console.error("\x1b[41m", "\x1b[37m", `Error into PagoAporteModel -> findAll()`, "\x1b[0m");
            return [];
        }
    }
}