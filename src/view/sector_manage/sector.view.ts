/**
 * Materia: Arquitectura de Software
 * UAGRM - FICCT
 * @author: Ruddy Bryan Quispe Mamani 
 * @version: 0.0.1
 * @since: 14-04-2021
 */

import { Response } from '../../options';
import { SectorModel } from '../../model/sector.model';

export class SectorView {

    /**
     * Attributes
     */
    private id: number;
    private descripcion: string;
    private sectorModel: SectorModel;

    /**
     * Constructor
     */
    constructor() {
        this.sectorModel = new SectorModel();
        this.id = -1;
        this.descripcion = "";
    }

    public setId = (id: number): void => { this.id = id; }
    public setDescripcion = (descripcion: string): void => { this.descripcion = descripcion; }

    public getId = (): number => this.id;
    public getDescripcion = (): string => this.descripcion;

    /**
     * update view Sector Manage
     * @param response : response HTTP
     */
    public async getSectorManageView(response: Response): Promise<void> {
        let sectorList = await this.sectorModel.findAll();
        response.render('sector_manage/sector_manage', {
            sector_list: sectorList
        });
    }

    /**
     * render view sector manage
     * @param response response http
     */
    public async redirectSectorManageView(response: Response): Promise<void> {
        response.redirect('/sector_manage');
    }

    /**
     * get edit sector edit
     * @param response : request HTTP
     * @param idSector id sector selected
     */
    public async getEditSectorView(response: Response, idSector: number): Promise<void> {
        this.sectorModel.setId(idSector);
        let sectorData: { id: number, descripcion: string } | undefined = await this.sectorModel.findById();
        if (sectorData) {
            // si existe categoria y los obtuvo
            response.status(200).render('sector_manage/sector_edit', sectorData);
        } else {
            // si no existe categoria con el idCategoria (undefined)
            this.getMessageErrorView(response, "No se encontro el sector seleccionado");
        }
    }

    /**
     * Render view error
     * @param response : HTTP response
     * @param message : message error to send
     */
    public async getMessageErrorView(response: Response, message: string): Promise<void> {
        response.render('error', { message_error: message });
    }
}