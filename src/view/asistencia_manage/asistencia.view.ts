/**
 * Materia: Arquitectura de Software
 * UAGRM - FICCT
 * @author: Ruddy Bryan Quispe Mamani 
 * @version: 0.0.1
 * @since: 14-04-2021
 */

import { AsistenciaModel } from '../../model/asistencia.model';
import { Response } from '../../options';


export class AsistenciaView {

    /**
     * Attributes
     */
    private codigo: number;
    private descripcion: string;
    private fechaRealizado: Date;
    private asistenciaModel: AsistenciaModel;

    /**
     * Constructor
     */
    constructor() {
        this.codigo = -1;
        this.descripcion = "-1";
        this.fechaRealizado = new Date();
        this.asistenciaModel = new AsistenciaModel();
    }

    public setCodigo = (codigo: number): void => { this.codigo = codigo; }
    public setDescripcion = (descripcion: string): void => { this.descripcion = descripcion; }
    public setFechaRealizado = (fechaRealizado: Date): void => { this.fechaRealizado = fechaRealizado; }

    public getCodigo = (): number => this.codigo;
    public getDescripcion = (): string => this.descripcion;
    public getFechaRealizado = (): Date => this.fechaRealizado;

    /**
     * update view Attendance Manage
     * @param response : response HTTP
     */
    public async getAsistenciaManageView(
        response: Response,
        listAssociates: Array<{ ci: number, nombre: string, telefono: string, direccion: string, fecha_afiliacion: string, estado: boolean, nro_puesto: number }>): Promise<void> {
        let listAttendance: Array<{ codigo: number, descripcion: string, fecha_realizado: string }>
            = await this.asistenciaModel.findAll();
        response.render('asistencia_manage/asistencia_manage', {
            list_attendance: listAttendance,
            list_associates: listAssociates
        });
    }

    /**
     * render view contribution manage
     * @param response response http
     */
    public async redirectAsistenciaManageView(response: Response): Promise<void> {
        response.redirect('/asistencia_manage');
    }

    /**
     * get List Asistencia Detail Associate
     * @param response : request HTTP
     * @param listAsistencia : response HTTP
     */
    public async getListAsistencia(response: Response, listAsistencia: Array<any>): Promise<void> {
        response.json({ list_asistencia: listAsistencia });
    }

    /**
     * get edit contribution edit
     * @param response : request HTTP
     * @param codigoAsistencia : code contribution selected
     */
    public async getEditAsistenciaView(response: Response, codigoAsistencia: number, listSocio: Array<{ ci: number, nombre: string, telefono: string, direccion: string, fecha_afiliacion: string, estado: boolean, nro_puesto: number, verify_asistencia: boolean }>): Promise<void> {
        this.asistenciaModel.setCodigo(codigoAsistencia);
        let asistenciaData: { codigo: number, descripcion: string, fecha_realizado: string } | undefined
            = await this.asistenciaModel.findById();
        if (asistenciaData) {
            // if exists contribution info
            response.status(200).render('asistencia_manage/asistencia_edit', {
                asistencia_data: asistenciaData,
                list_socio: listSocio
            });
        } else {
            // if don't exists contribution
            this.getMessageErrorView(response, "No se encontro los datos");
        }
    }

    /**
     * Render view error
     * @param response : HTTP response
     * @param message : message error to send
     */
    public async getMessageErrorView(response: Response, message: string): Promise<void> {
        response.render('error', { message_error: message });
    }
}