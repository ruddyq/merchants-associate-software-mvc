/**
 * Materia: Software Architecture
 * UAGRM - FICCT
 * @author: Ruddy Bryan Quispe Mamani 
 * @version: 1.0.0
 * @since: 29-10-2021
 */

/**
 * Libraries Import
 */
import { QueryResult } from "pg";
import { Connection } from "../database/connection";

/**
 * @class Attendance Associate by Model Package contained MVC architecture
 */
export class AsistenciaSocioModel {

    /**
     * Attributes
     */
    private ciSocio: number;
    private codAsistencia: number;
    private connectionSQL: Connection;

    /**
     * @constructor
     */
    constructor() {
        this.ciSocio = -1;
        this.codAsistencia = -1;
        this.connectionSQL = Connection.getInstance();
    }

    public setCiSocio = (ciSocio: number): void => { this.ciSocio = ciSocio; }
    public setCodAsistencia = (codAsistencia: number): void => { this.codAsistencia = codAsistencia; }

    public getCiSocio = (): number => this.ciSocio;
    public getCodAsistencia = (): number => this.codAsistencia;

    /**
     * create a new Attendance-Associate register
     * @returns true if saved successfully
     */
    public create = async (): Promise<boolean> => {
        try {
            await this.connectionSQL.executeSQL(
                `insert into asistencia_socio(ci_socio, cod_asistencia) 
                    values (${this.ciSocio}, ${this.codAsistencia});`
            );
            return true;
        } catch (error) {
            console.log("\x1b[41m", "\x1b[37m", `Error into AsistenciaSocioModel -> create()`, "\x1b[0m");
            return false;
        }
    }

    /**
     * find by ciSocio Attendance saved
     * @returns object Attendance or undefined if have an error or not find
     */
    public findById = async (): Promise<{ ci_socio: number, nombre: string, cod_asistencia: number, fecha_realizado: string }
        | undefined> => {
        try {
            let AttendanceObtained: QueryResult =
                await this.connectionSQL.executeSQL(
                    `select as2.ci_socio, s.nombre, as2.cod_asistencia, a.fecha_realizado 
                    from asistencia_socio as2, asistencia a, socio s 
                    where as2.ci_socio=s.ci and as2.cod_asistencia=a.codigo and as2.ci_socio=${this.ciSocio} and as2.cod_asistencia=${this.codAsistencia}`
                );
            return AttendanceObtained.rows[0];
        } catch (error) {
            console.log("\x1b[41m", "\x1b[37m", `Error into AsistenciaSocioModel -> findById()`, "\x1b[0m");
            return undefined;
        }
    }

    /**
     * remove an Attendance of ciSocio assigned AsistenciaSocioModel.ciSocio
     * @returns true if Attendance was removed, return false if has a trouble
     */
    public delete = async (): Promise<boolean> => {
        try {
            await this.connectionSQL.executeSQL(
                `delete from asistencia_socio where ci_socio=${this.ciSocio} and cod_asistencia=${this.codAsistencia};`
            );
            return true;
        } catch (error) {
            console.log("\x1b[41m", "\x1b[37m", `Error into AsistenciaSocioModel -> delete()`, "\x1b[0m");
            return false;
        }
    }

    /**
     * clean detail asistencia
     * @returns delete all asistencia
     */
    public deleteAsistenciaDetail = async (): Promise<boolean> => {
        try {
            await this.connectionSQL.executeSQL(
                `delete from asistencia_socio where cod_asistencia=${this.codAsistencia};`
            );
            return true;
        } catch (error) {
            console.log("\x1b[41m", "\x1b[37m", `Error into AsistenciaSocioModel -> delete()`, "\x1b[0m");
            return false;
        }
    }

    /**
     * get list Attendance saved
     * @returns Array data Attendance
     */
    public findAll = async (): Promise<Array<{ ci_socio: number, nombre: string, cod_asistencia: number, fecha_realizado: string }>> => {
        try {
            let AttendanceList = await this.connectionSQL.executeSQL(
                `select as2.ci_socio, s.nombre, as2.cod_asistencia, a.fecha_realizado 
                from asistencia_socio as2, asistencia a, socio s 
                where as2.ci_socio=s.ci 
                    and as2.cod_asistencia=a.codigo 
                    and as2.cod_asistencia=${this.codAsistencia} 
                order by s.nombre;`
            );
            return AttendanceList.rows;
        } catch (error) {
            console.log("\x1b[41m", "\x1b[37m", `Error into AsistenciaSocioModel -> findAll()`, "\x1b[0m");
            return [];
        }
    }
}