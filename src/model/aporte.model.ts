/**
 * Materia: Software Architecture
 * UAGRM - FICCT
 * @author: Ruddy Bryan Quispe Mamani 
 * @version: 1.0.0
 * @since: 25-10-2021
 */

/**
 * Libraries Import
 */
import { QueryResult } from "pg";
import { Connection } from "../database/connection";

/**
 * @class Contribution by Model Package contained MVC Architecture
 */
export class AporteModel {

    /**
     * Attributes
     */
    private codigo: string;
    private descripcion: string;
    private fecha: Date;
    private monto: number;
    private connectionSQL: Connection;

    /**
     * @constructor
     */
    constructor() {
        this.codigo = "";
        this.descripcion = "";
        this.fecha = new Date();
        this.monto = 0.00;
        this.connectionSQL = Connection.getInstance();
    }

    public setCodigo = (codigo: string): void => { this.codigo = codigo; }
    public setDescripcion = (descripcion: string): void => { this.descripcion = descripcion; }
    public setFecha = (fecha: Date): void => { this.fecha = fecha; }
    public setMonto = (monto: number): void => { this.monto = monto; }

    public getCodigo = (): string => this.codigo;
    public getDescripcion = (): string => this.descripcion;
    public getFecha = (): Date => this.fecha;
    public getMonto = (): number => this.monto;

    /**
     * create a new Contribution
     * @returns true if saved successfully
     */
    public create = async (): Promise<boolean> => {
        try {
            await this.connectionSQL.executeSQL(`insert into aporte 
                (codigo, descripcion, fecha_creacion, monto) values 
                ('${this.codigo}', '${this.descripcion}','${this.fecha}', ${this.monto});`);
            return true;
        } catch (error) {
            console.log("\x1b[41m", "\x1b[37m", `Error into AporteModel -> create()`, "\x1b[0m");
            return false;
        }
    }

    /**
     * find by code contribution saved
     * @returns object contribution or undefined if have an error or not find
     */
    public findById = async (): Promise<{ codigo: string; descripcion: string, fecha_creacion: string, monto: number } | undefined> => {
        try {
            let contributionObtained: QueryResult = await this.connectionSQL.
                executeSQL(`select codigo, descripcion, to_char(fecha_creacion ::DATE,'yyyy-mm-dd') as fecha_creacion, monto 
                    from aporte 
                    where codigo='${this.codigo}';`);
            return contributionObtained.rows[0];
        } catch (error) {
            console.log("\x1b[41m", "\x1b[37m", `Error into AporteModel -> findById()`, "\x1b[0m");
            return undefined;
        }
    }

    /**
     * update info of a contribution with code assigned
     * @returns true if updated else return false
     */
    public update = async (): Promise<boolean> => {
        try {
            await this.connectionSQL.executeSQL(`update aporte 
                set descripcion='${this.descripcion}', fecha_creacion='${this.fecha}', monto=${this.monto} 
                where codigo='${this.codigo}';`);
            return true;
        } catch (error) {
            console.log("\x1b[41m", "\x1b[37m", `Error into AporteModel -> update()`, "\x1b[0m");
            return false;
        }
    }

    /**
     * remove an contribution of code assigned
     * @returns true if contribution was removed, return false if has a trouble
     */
    public delete = async (): Promise<boolean> => {
        try {
            await this.connectionSQL.executeSQL(`delete from aporte 
                where codigo='${this.codigo}';`);
            return true;
        } catch (error) {
            console.log("\x1b[41m", "\x1b[37m", `Error into AporteModel -> delete()`, "\x1b[0m");
            return false;
        }
    }

    /**
     * get list contribution saved
     * @returns Array data sector
     */
    public findAll = async (): Promise<Array<{ codigo: string; descripcion: string, fecha_creacion: string, monto: number }>> => {
        try {
            let sectorList = await this.connectionSQL.executeSQL(`select codigo, descripcion, to_char(fecha_creacion ::DATE,'Day dd Mon yyyy') as fecha_creacion, monto 
                from aporte 
                order by codigo;`);
            return sectorList.rows;
        } catch (error) {
            console.log("\x1b[41m", "\x1b[37m", `Error into AporteModel -> findAll()`, "\x1b[0m");
            return [];
        }
    }
}