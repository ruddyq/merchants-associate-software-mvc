/**
 * Materia: Software Architecture
 * UAGRM - FICCT
 * @author: Ruddy Bryan Quispe Mamani 
 * @version: 1.0.0
 * @since: 26-10-2021
 */

/**
 * Libraries Import
 */
import { QueryResult } from "pg";
import { Connection } from "../database/connection";

/**
 * @class Socio by Model package contained MVC architecture
 */
export class SocioModel {

    /**
     * Attributes
     */
    private ci: number;
    private nombre: string;
    private telefono: string;
    private direccion: string;
    private fechaAfiliacion: Date;
    private estado: boolean;
    private nroPuesto: number | undefined;
    private connectionSQL: Connection;

    /**
     * @constructor
     */
    constructor() {
        this.ci = -1;
        this.nombre = "";
        this.telefono = "";
        this.direccion = "";
        this.fechaAfiliacion = new Date();
        this.estado = false;
        this.nroPuesto = undefined;
        this.connectionSQL = Connection.getInstance();
    }

    public setCI = (ci: number): void => { this.ci = ci; }
    public setNombre = (nombre: string): void => { this.nombre = nombre; }
    public setTelefono = (telefono: string): void => { this.telefono = telefono; }
    public setDireccion = (direccion: string): void => { this.direccion = direccion; }
    public setFechaAfiliacion = (fechaAfiliacion: Date): void => { this.fechaAfiliacion = fechaAfiliacion; }
    public setEstado = (estado: boolean): void => { this.estado = estado; }
    public setNroPuesto = (nroPuesto: number | undefined): void => { this.nroPuesto = nroPuesto; }

    public getCI = (): number => this.ci;
    public getNombre = (): string => this.nombre;
    public getTelefono = (): string => this.telefono;
    public getDireccion = (): string => this.direccion;
    public getFechaAfiliacion = (): Date => this.fechaAfiliacion;
    public getEstado = (): boolean => this.estado;
    public getNroPuesto = (): number | undefined => this.nroPuesto;

    /**
     * create a new associate
     * @returns true if saved successfully, else return false
     */
    public create = async (): Promise<boolean> => {
        try {
            let nroPuesto = this.nroPuesto ? this.nroPuesto : "null";
            await this.connectionSQL.executeSQL(
                `insert into 
                socio(ci,nombre,telefono,direccion,fecha_afiliacion,estado,nro_puesto)
                values (${this.ci}, '${this.nombre}', '${this.telefono}', '${this.direccion}', '${this.fechaAfiliacion}', ${this.estado}, ${nroPuesto});`
            );
            return true;
        } catch (error) {
            console.log("\x1b[41m", "\x1b[37m", `Error into SocioModel -> create()`, "\x1b[0m");
            return false;
        }
    }

    /**
     * find by id shop saved
     * @returns object shop or undefined if have an error or not find
     */
    public findById = async (): Promise<{ ci: number, nombre: string, telefono: string, direccion: string, fecha_afiliacion: string, estado: boolean, nro_puesto: number } | undefined> => {
        try {
            let shopObtained: QueryResult = await this.connectionSQL.executeSQL(
                `select ci, nombre, telefono, direccion, to_char(fecha_afiliacion ::DATE,'yyyy-mm-dd') as fecha_afiliacion, estado, nro_puesto 
                from socio
                where ci=${this.ci};`
            );
            return shopObtained.rows[0];
        } catch (error) {
            console.log("\x1b[41m", "\x1b[37m", `Error into SocioModel -> findById()`, "\x1b[0m");
            return undefined;
        }
    }

    /**
     * update info to associate selected
     * @returns true if updated else return false
     */
    public update = async (): Promise<boolean> => {
        try {
            let nroPuesto = this.nroPuesto ? this.nroPuesto : "null";
            await this.connectionSQL.executeSQL(
                `update socio 
                set nombre='${this.nombre}', telefono='${this.telefono}', direccion='${this.direccion}', 
                    fecha_afiliacion='${this.fechaAfiliacion}', estado=${this.estado}, nro_puesto=${nroPuesto} where ci=${this.ci};`
            );
            return true;
        } catch (error) {
            console.log("\x1b[41m", "\x1b[37m", `Error into SocioModel -> update()`, "\x1b[0m");
            return false;
        }
    }

    /**
     * remove an shop of shop No assigned
     * @returns true if sector was removed, return false if has a trouble
     */
    public delete = async (): Promise<boolean> => {
        try {
            await this.connectionSQL.executeSQL(
                `delete from socio 
                where ci=${this.ci};`
            );
            return true;
        } catch (error) {
            console.log("\x1b[41m", "\x1b[37m", `Error into SocioModel -> delete()`, "\x1b[0m");
            return false;
        }
    }

    /**
     * get list shop saved
     * @returns Array data sector
     */
    public findAll = async (): Promise<Array<{ ci: number, nombre: string, telefono: string, direccion: string, fecha_afiliacion: string, estado: boolean, nro_puesto: number }>> => {
        try {
            let shopList = await this.connectionSQL.executeSQL(
                `select ci, nombre, telefono, direccion, to_char(fecha_afiliacion ::DATE,'Day dd Mon yyyy') as fecha_afiliacion, estado, nro_puesto 
                from socio
                order by fecha_afiliacion desc;`);
            return shopList.rows;
        } catch (error) {
            console.log("\x1b[41m", "\x1b[37m", `Error into SocioModel -> findAll()`, "\x1b[0m");
            return [];
        }
    }

    /**
     * enable or disable one associate
     * @returns true if change its estatus
     */
    public updatePermitOfAssociate = async (): Promise<boolean> => {
        try {
            await this.connectionSQL.executeSQL(
                `update socio 
                    set estado=not estado
                    where ci=${this.ci};`
            );
            return true;
        } catch (error) {
            console.log("\x1b[41m", "\x1b[37m", `Error into SocioModel -> updatePermitOfAssociate()`, "\x1b[0m");
            return false;
        }
    }
}