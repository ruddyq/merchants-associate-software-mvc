# SOFTWARE DE GESTION DE APORTES PARA LA ASOCIACION DE UN MERCADO

_Project for the subject of SOFTWARE ARCHITECTURE at the UAGRM-FICCT._
_SOFTWARE DE GESTION DE APORTES PARA LA ASOCIACION DE UN MERCADO_

## Comenzando 🚀

_A continuación, estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._

Mira **Deployment** para conocer como desplegar el proyecto.

### Pre-requisitos 📋

_Que cosas necesitas para instalar el software y como instalarlas_

```
Node JS version 14.18.0 o LTS
npm gestor de paquetes
PostgreSQL
Navegador Web
```

### Instalación 🔧

_Cargar la BD en un motor SQL PostgreSQL_

_\* el codigo SQL se encuentra en ./src/database/sql/script.sql_

_Paso a paso que te dice lo que debes ejecutar para tener un entorno de desarrollo ejecutandose_

```
cd merchants-associate-software-mvc
npm install
npm run dev
```

_Y detener la compilación_

```
Ctrl+c
```

## Ejecutando las pruebas ⚙️

_Ya esta explicado alli arriba_

### Analice las pruebas end-to-end 🔩

_No se realizó ninguna prueba_

### Y las pruebas de estilo de codificación ⌨️

_Este software web solamente 1 parte conformado con 3 paquetes o módulos_

## Despliegue 📦

_pm2 (pero no se realizó ningun despliegue aun, todo esta en proceso de desarrollo)_

## Construido con 🛠️

_herramientas que utiliza para crear tu proyecto_

- [Windows o Linux](http://google.com/) - Cualquier Sistema Operativo, pero preferible Linux
- [NodeJS](https://nodejs.org/en/) - Compilador de JS en Desktop
- [PostgreSQL](https://www.postgresql.org/) - Motor de Base de Datos

## Contribuyendo 🖇️

Por favor lee el [Documentación](https://google.com) para detalles de nuestro código de conducta, y el proceso para enviarnos pull requests.

## Versionado 📌

Usamos [Git](http://git.org/) para el versionado - [Bitbucket](https://bitbucket.org/ruddyq/merchants-associate-software-mvc/src/main/) para el contenedor de repositorios

## Autores ✒️

_Agradecido con el de Arriba_

- **Quispe Mamani Ruddy Bryan** - _Development Jr_ - [Github](https://github.com/RuddyQuispe) [Bitbucket](https://bitbucket.org/ruddyq/)

## Expresiones de Gratitud 🎁

- Agradecido con el de Arriba 📢
- Este proyecto tiene fines de aportar con la comunidad de software libre

---

⌨️ con ❤️ por Ruddy 😊
