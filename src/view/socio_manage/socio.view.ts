/**
 * Materia: Arquitectura de Software
 * UAGRM - FICCT
 * @author: Ruddy Bryan Quispe Mamani 
 * @version: 0.0.1
 * @since: 27-10-2021
 */

import { SocioModel } from '../../model/socio.model';
import { Response } from '../../options';

export class SocioView {

    /**
     * Attributes
     */
    private ci: number;
    private nombre: string;
    private telefono: string;
    private direccion: string;
    private fechaAfiliacion: Date;
    private estado: boolean;
    private nroPuesto: number | undefined;
    private socioModel: SocioModel;

    /**
     * Constructor
     */
    constructor() {
        this.ci = -1;
        this.nombre = "";
        this.telefono = "";
        this.direccion = "";
        this.fechaAfiliacion = new Date();
        this.estado = false;
        this.nroPuesto = undefined;
        this.socioModel = new SocioModel();
    }

    public setCI = (ci: number): void => { this.ci = ci; }
    public setNombre = (nombre: string): void => { this.nombre = nombre; }
    public setTelefono = (telefono: string): void => { this.telefono = telefono; }
    public setDireccion = (direccion: string): void => { this.direccion = direccion; }
    public setFechaAfiliacion = (fechaAfiliacion: Date): void => { this.fechaAfiliacion = fechaAfiliacion; }
    public setEstado = (estado: boolean): void => { this.estado = estado; }
    public setNroPuesto = (nroPuesto: number): void => { this.nroPuesto = nroPuesto; }

    public getCI = (): number => this.ci;
    public getNombre = (): string => this.nombre;
    public getTelefono = (): string => this.telefono;
    public getDireccion = (): string => this.direccion;
    public getFechaAfiliacion = (): Date => this.fechaAfiliacion;
    public getEstado = (): boolean => this.estado;
    public getNroPuesto = (): number | undefined => this.nroPuesto;

    /**
     * update view Socio Manage
     * @param response : response HTTP
     */
    public async getAssociateManageView(response: Response, puestoList: Array<{ nro_puesto: number, estado: boolean, id_sector: number, descripcion: string }>): Promise<void> {
        let socioList = await this.socioModel.findAll();
        response.render('socio_manage/socio_manage', {
            socio_list: socioList,
            puesto_list: puestoList
        });
    }

    /**
     * render view Socio manage
     * @param response response http
     */
    public async redirectAssociateManageView(response: Response): Promise<void> {
        response.redirect('/socio_manage');
    }

    /**
     * get edit socio edit
     * @param response : request HTTP
     * @param socioCI : socio CI selected
     */
    public async getEditAssociateView(response: Response, socioCI: number, puestoList: Array<any>): Promise<void> {
        this.socioModel.setCI(socioCI);
        let socioData: { ci: number, nombre: string, telefono: string, direccion: string, fecha_afiliacion: string, estado: boolean, nro_puesto: number } | undefined
            = await this.socioModel.findById();
        if (socioData) {
            // if associate exists
            response.status(200).render('socio_manage/socio_edit', {
                socio_data: socioData,
                puesto_list: puestoList
            });
        } else {
            // if don't exists associate
            this.getMessageErrorView(response, "No se encontro los datos del socio seleccionado");
        }
    }

    /**
     * Render view error
     * @param response : HTTP response
     * @param message : message error to send
     */
    public async getMessageErrorView(response: Response, message: string): Promise<void> {
        response.render('error', { message_error: message });
    }
}