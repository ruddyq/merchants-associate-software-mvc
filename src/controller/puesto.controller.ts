/**
 * Materia: Arquitectura de Software
 * UAGRM - FICCT
 * @author: Ruddy Bryan Quispe Mamani 
 * @version: 0.0.1
 * @since: 26-10-2021
 */

import { PuestoModel } from '../model/puesto.model';
import { SectorModel } from '../model/sector.model';
import { Request, Response, Router } from '../options';
import { PuestoView } from '../view/puesto_manage/puesto.view';

export class PuestoController {
    /**
     * Attributes
     */
    public router: Router;
    private puestoModel: PuestoModel;
    private puestoView: PuestoView;
    private sectorModel: SectorModel;

    /**
     * Constructor
     */
    constructor() {
        this.router = Router();
        this.createRoutes();
        this.puestoModel = new PuestoModel();
        this.puestoView = new PuestoView();
        this.sectorModel = new SectorModel();
    }

    /**
     * return list shop with views
     * @param request : request de HTTP
     * @param response : response de HTTP
     */
    public async getViewShopManage(request: Request, response: Response): Promise<void> {
        let sectorList: Array<{ id: number, descripcion: string }> = await this.sectorModel.findAll();
        await this.puestoView.getShopManageView(response, sectorList);
    }

    /**
     * create new shop
     * @param request : request HTTP
     * @param response : response HTTP
     */
    public async createShop(request: Request, response: Response): Promise<void> {
        let { nro_puesto, id_sector } = request.body;
        let status = request.body.estado ? true : false;
        this.puestoModel.setNroPuesto(parseInt(nro_puesto));
        this.puestoModel.setEstado(status);
        this.puestoModel.setIdSector(parseInt(id_sector));
        let created = await this.puestoModel.create();
        if (created) {
            await this.puestoView.redirectShopManageView(response);
        } else {
            await this.puestoView.getMessageErrorView(response, "No se pudo guardar el nuevo puesto");
        }
    }

    /**
     * get a shop specified
     * @param request : request HTTP
     * @param response : response HTTP
     */
    public async getViewEditShop(request: Request, response: Response): Promise<void> {
        let { nro_puesto } = request.params;
        let sectorList: Array<{ id: number, descripcion: string }> = await this.sectorModel.findAll();
        await this.puestoView.getEditShopView(response, parseInt(nro_puesto), sectorList);
    }

    /**
     * update a shop
     * @param request : request HTTP
     * @param response : response HTTP
     */
    public async updateShop(request: Request, response: Response): Promise<void> {
        let { nro_puesto } = request.params;
        let { estado, id_sector } = request.body;
        this.puestoModel.setNroPuesto(parseInt(nro_puesto));
        this.puestoModel.setEstado(estado ? true : false);
        this.puestoModel.setIdSector(parseInt(id_sector));
        let updated: boolean = await this.puestoModel.update();
        if (updated) {
            // update shop successfully
            this.puestoView.redirectShopManageView(response);
        } else {
            // it has trouble to update shop
            this.puestoView.getMessageErrorView(response, "No se pudo modificar los datos del puesto " + nro_puesto);
        }
    }

    /**
     * remove one shop
     * @param request : request HTTP
     * @param response : response HTTP
     */
    public async deleteShop(request: Request, response: Response): Promise<void> {
        let { nro_puesto } = request.params;
        this.puestoModel.setNroPuesto(parseInt(nro_puesto));
        let deleted = await this.puestoModel.delete();
        if (deleted) {
            let sectorList: Array<{ id: number, descripcion: string }> = await this.sectorModel.findAll();
            await this.puestoView.getShopManageView(response, sectorList);
        } else {
            this.puestoView.getMessageErrorView(response, "No se pudo eliminar el puesto Nro. " + nro_puesto);
        }
    }

    /**
     * change status of a shop
     * @param request : HTTP Request
     * @param response : HTTP Response
     */
    public async permitShop(request: Request, response: Response): Promise<void> {
        let { nro_puesto } = request.params;
        this.puestoModel.setNroPuesto(parseInt(nro_puesto));
        let permitShop = await this.puestoModel.updatePermitOfShop();
        if (permitShop) {
            let sectorList = await this.sectorModel.findAll();
            await this.puestoView.getShopManageView(response, sectorList);
        } else {
            this.puestoView.getMessageErrorView(response,
                "No se pudo cambiar el estado del puesto Nro. " + nro_puesto);
        }
    }

    /**
     * metodo privado para cargar las rutas que disponen en los metodos HTTP
     */
    private createRoutes(): void {
        this.router.route('/').get(async (req: Request, res: Response) => this.getViewShopManage(req, res));
        this.router.route('/').post(async (req: Request, res: Response) => this.createShop(req, res));
        this.router.route('/:nro_puesto').get(async (req: Request, res: Response) => this.getViewEditShop(req, res));
        this.router.route('/update/:nro_puesto').put(async (req: Request, res: Response) => this.updateShop(req, res));
        this.router.route('/delete/:nro_puesto').delete(async (req: Request, res: Response) => this.deleteShop(req, res));
        this.router.route('/permit/:nro_puesto').put(async (req: Request, res: Response) => this.permitShop(req, res));
    }
}