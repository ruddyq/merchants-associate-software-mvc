/**
 * Materia: Software Architecture
 * UAGRM - FICCT
 * @author: Ruddy Bryan Quispe Mamani 
 * @version: 1.0.0
 * @since: 09-10-2021
 */

/**
 * Libraries Import
 */
import { QueryResult } from "pg";
import { Connection } from "../database/connection";

/**
 * @class Sector by Model Package contained MVC architecture
 */
export class SectorModel {

    /**
     * Attributes
     */
    private id: number;
    private descripcion: string;
    private connectionSQL: Connection;

    /**
     * @constructor
     */
    constructor() {
        this.id = -1;
        this.descripcion = "";
        this.connectionSQL = Connection.getInstance();
    }

    public setId = (id: number): void => { this.id = id; }
    public setDescripcion = (descripcion: string): void => { this.descripcion = descripcion; }

    public getId = (): number => this.id;
    public getDescripcion = (): string => this.descripcion;

    /**
     * create a new sector
     * @returns true if saved successfully
     */
    public create = async (): Promise<boolean> => {
        try {
            await this.connectionSQL.executeSQL(`insert into sector(descripcion) values ('${this.descripcion}');`);
            return true;
        } catch (error) {
            console.log("\x1b[41m", "\x1b[37m", `Error into SectorModelo -> create()`, "\x1b[0m");
            return false;
        }
    }

    /**
     * find by id sector saved
     * @returns object sector or undefined if have an error or not find
     */
    public findById = async (): Promise<{ id: number; descripcion: string; } | undefined> => {
        try {
            let sectorObtained: QueryResult = await this.connectionSQL.executeSQL(`select id, descripcion from sector where id=${this.id};`);
            return sectorObtained.rows[0];
        } catch (error) {
            console.log("\x1b[41m", "\x1b[37m", `Error into SectorData -> findById()`, "\x1b[0m");
            return undefined;
        }
    }

    /**
     * update descripcion of a sector with ID assigned
     * @returns true if updated else return false
     */
    public update = async (): Promise<boolean> => {
        try {
            await this.connectionSQL.executeSQL(`update sector set descripcion='${this.descripcion}' where id=${this.id};`);
            return true;
        } catch (error) {
            console.log("\x1b[41m", "\x1b[37m", `Error into SectorData -> update()`, "\x1b[0m");
            return false;
        }
    }

    /**
     * remove an sector of id assigned SectorData.id
     * @returns true if sector was removed, return false if has a trouble
     */
    public delete = async (): Promise<boolean> => {
        try {
            await this.connectionSQL.executeSQL(`delete from sector where id=${this.id};`);
            return true;
        } catch (error) {
            console.log("\x1b[41m", "\x1b[37m", `Error into SectorData -> delete()`, "\x1b[0m");
            return false;
        }
    }

    /**
     * get list sector saved
     * @returns Array data sector
     */
    public findAll = async (): Promise<Array<{ id: number, descripcion: string }>> => {
        try {
            let sectorList = await this.connectionSQL.executeSQL(`select id, descripcion from sector order by id;`);
            return sectorList.rows;
        } catch (error) {
            console.log("\x1b[41m", "\x1b[37m", `Error into SectorData -> findAll()`, "\x1b[0m");
            return [];
        }
    }
}