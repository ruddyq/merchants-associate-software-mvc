/**
 * Materia: Arquitectura de Software
 * UAGRM - FICCT
 * @author: Ruddy Bryan Quispe Mamani 
 * @version: 0.0.1
 * @since: 29-10-2021
 */

import { AporteModel } from '../model/aporte.model';
import { PagoAporteModel } from '../model/pago_aporte.model';
import { SocioModel } from '../model/socio.model';
import { Request, Response, Router } from '../options';
import { PagoAporteView } from '../view/pago_aporte_manage/pago_aporte.view';


export class PagoAporteController {
    /**
     * Attributes
     */
    public router: Router;
    private pagoAporteModel: PagoAporteModel;
    private aporteModel: AporteModel;
    private socioModel: SocioModel;
    private pagoAporteView: PagoAporteView;

    /**
     * Constructor
     */
    constructor() {
        this.router = Router();
        this.createRoutes();
        this.pagoAporteModel = new PagoAporteModel();
        this.socioModel = new SocioModel();
        this.aporteModel = new AporteModel();
        this.pagoAporteView = new PagoAporteView();
    }

    /**
     * return list associate with views and options post
     * @param request : request de HTTP
     * @param response : response de HTTP
     */
    public async getViewPagoAporteManage(request: Request, response: Response): Promise<void> {
        let listSocio = await this.socioModel.findAll();
        let listAporte = await this.aporteModel.findAll();
        await this.pagoAporteView.getPagoAporteManageView(response, listSocio, listAporte);
    }

    /**
     * create new attendance
     * @param request : request HTTP
     * @param response : response HTTP
     */
    public async createPagoAporte(request: Request, response: Response): Promise<void> {
        let { monto_total, fecha_pago, ci_socio, list_detail_aport } = request.body;
        this.pagoAporteModel.setMontoTotal(parseInt(monto_total));
        this.pagoAporteModel.setFechaPago(fecha_pago);
        this.pagoAporteModel.setCiSocio(parseInt(ci_socio));
        this.pagoAporteModel.setListaDetallePago(list_detail_aport, -1);
        let created: boolean = await this.pagoAporteModel.create();
        if (created) {
            await this.pagoAporteView.redirectPagoAporteManageView(response);
        } else {
            await this.pagoAporteView.getMessageErrorView(response, "No se pudo guardar el nuevo pago aporte");
        }
    }

    /**
     * get a contribution specified
     * @param request : request HTTP
     * @param response : response HTTP
     */
    public async getViewEditPagoAporte(request: Request, response: Response): Promise<void> {
        let { nro_pago } = request.params;
        this.pagoAporteModel.setNro(parseInt(nro_pago));
        let listSocio: Array<{ ci: number, nombre: string, telefono: string, direccion: string, fecha_afiliacion: string, estado: boolean, nro_puesto: number }>
            = await this.socioModel.findAll();
        let listAporte: Array<{ codigo: string, descripcion: string, fecha_creacion: string, monto: number }>
            = await this.aporteModel.findAll();
        let listDetailAportePago: Array<{ codigo: string, descripcion: string, monto: number, nro_pago: number }> | undefined
            = await this.pagoAporteModel.findDetailById();
        await this.pagoAporteView.getEditPagoAporteView(response, parseInt(nro_pago), listSocio, listAporte, listDetailAportePago);
    }

    /**
     * update a associate
     * @param request : request HTTP
     * @param response : response HTTP
     */
    public async updatePagoAporte(request: Request, response: Response): Promise<void> {
        let { nro_pago } = request.params;
        let { monto_total, fecha_pago, ci_socio, list_detail_aport } = request.body;
        this.pagoAporteModel.setNro(parseInt(nro_pago));
        this.pagoAporteModel.setFechaPago(fecha_pago);
        this.pagoAporteModel.setMontoTotal(monto_total);
        this.pagoAporteModel.setCiSocio(ci_socio);
        this.pagoAporteModel.setListaDetallePago(list_detail_aport, parseInt(nro_pago));
        let updated: boolean = await this.pagoAporteModel.update();
        if (updated) {
            let listSocio = await this.socioModel.findAll();
            let listAporte = await this.aporteModel.findAll();
            await this.pagoAporteView.getPagoAporteManageView(response, listSocio, listAporte);
        } else {
            await this.pagoAporteView.getMessageErrorView(response, "No se pudo modificar el pago aporte Nro." + nro_pago + ", intente nuevamente por favor");
        }
    }

    /**
     * remove one associate
     * @param request : request HTTP
     * @param response : response HTTP
     */
    public async deletePagoAporte(request: Request, response: Response): Promise<void> {
        let { nro_pago } = request.params;
        this.pagoAporteModel.setNro(parseInt(nro_pago));
        this.pagoAporteModel.setListaDetallePago([{ cod_aport: '-1', monto: '0' }], parseInt(nro_pago));
        await this.pagoAporteModel.getListaDetallePago()[0].deleteAll();
        let deleted: boolean = await this.pagoAporteModel.delete();
        if (deleted) {
            let listSocio = await this.socioModel.findAll();
            let listAporte = await this.aporteModel.findAll();
            await this.pagoAporteView.getPagoAporteManageView(response, listSocio, listAporte);
        } else {
            this.pagoAporteView.getMessageErrorView(response, "No se pudo eliminar el Pago Nro: " + nro_pago);
        }
    }

    /**
     * metodo privado para cargar las rutas que disponen en los metodos HTTP
     */
    private createRoutes(): void {
        this.router.route('/').get(async (req: Request, res: Response) => this.getViewPagoAporteManage(req, res));
        this.router.route('/').post(async (req: Request, res: Response) => this.createPagoAporte(req, res));
        this.router.route('/:nro_pago').get(async (req: Request, res: Response) => this.getViewEditPagoAporte(req, res));
        this.router.route('/update/:nro_pago').put(async (req: Request, res: Response) => this.updatePagoAporte(req, res));
        this.router.route('/delete/:nro_pago').delete(async (req: Request, res: Response) => this.deletePagoAporte(req, res));
    }
}