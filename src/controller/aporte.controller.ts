/**
 * Materia: Arquitectura de Software
 * UAGRM - FICCT
 * @author: Ruddy Bryan Quispe Mamani 
 * @version: 0.0.1
 * @since: 25-10-2021
 */

import { AporteModel } from '../model/aporte.model';
import { Request, Response, Router } from '../options';
import { AporteView } from '../view/aporte_manage/aporte.view';

export class AporteController {
    /**
     * Attributes
     */
    public router: Router;
    private aporteModel: AporteModel;
    private aporteView: AporteView;

    /**
     * Constructor
     */
    constructor() {
        this.router = Router();
        this.createRoutes();
        this.aporteModel = new AporteModel();
        this.aporteView = new AporteView();
    }

    /**
     * return list contribution with views
     * @param request : request de HTTP
     * @param response : response de HTTP
     */
    public async getViewAporteManage(request: Request, response: Response): Promise<void> {
        await this.aporteView.getAporteManageView(response);
    }

    /**
     * create new contribution
     * @param request : request HTTP
     * @param response : response HTTP
     */
    public async createAporte(request: Request, response: Response): Promise<void> {
        let { codigo, descripcion, fecha, monto } = request.body;
        this.aporteModel.setCodigo(codigo);
        this.aporteModel.setDescripcion(descripcion);
        this.aporteModel.setFecha(fecha);
        this.aporteModel.setMonto(monto);
        let created: boolean = await this.aporteModel.create();
        if (created) {
            await this.aporteView.redirectAporteManageView(response);
        } else {
            this.aporteView.getMessageErrorView(response, "No se pudo guardar elnuevo aporte");
        }
    }

    /**
     * get a contribution specified
     * @param request : request HTTP
     * @param response : response HTTP
     */
    public async getViewEditAporte(request: Request, response: Response): Promise<void> {
        let { codigo } = request.params;
        await this.aporteView.getEditAporteView(response, codigo);
    }

    /**
     * update a contribution
     * @param request : request HTTP
     * @param response : response HTTP
     */
    public async updateAporte(request: Request, response: Response): Promise<void> {
        let { codigo } = request.params;
        let { descripcion, fecha, monto } = request.body;
        this.aporteModel.setCodigo(codigo);
        this.aporteModel.setDescripcion(descripcion);
        this.aporteModel.setFecha(fecha);
        this.aporteModel.setMonto(monto);
        let updated: boolean = await this.aporteModel.update();
        if (updated) {
            // updated contribution success
            this.aporteView.redirectAporteManageView(response);
        } else {
            // has errors to update contribution
            this.aporteView.getMessageErrorView(response, "No se pudo actualizar el aporte");
        }
    }

    /**
     * remove one contribution
     * @param request : request HTTP
     * @param response : response HTTP
     */
    public async deleteAporte(request: Request, response: Response): Promise<void> {
        let { codigo } = request.params;
        this.aporteModel.setCodigo(codigo);
        let deleted: boolean = await this.aporteModel.delete();
        if (deleted) {
            this.aporteView.getAporteManageView(response);
        } else {
            this.aporteView.getMessageErrorView(response, "No se pudo eliminar el aporte");
        }
    }

    /**
     * metodo privado para cargar las rutas que disponen en los metodos HTTP
     */
    private createRoutes(): void {
        this.router.route('/').get(async (req: Request, res: Response) => this.getViewAporteManage(req, res));
        this.router.route('/').post(async (req: Request, res: Response) => this.createAporte(req, res));
        this.router.route('/:codigo').get(async (req: Request, res: Response) => this.getViewEditAporte(req, res));
        this.router.route('/update/:codigo').put(async (req: Request, res: Response) => this.updateAporte(req, res));
        this.router.route('/delete/:codigo').delete(async (req: Request, res: Response) => this.deleteAporte(req, res));
    }
}