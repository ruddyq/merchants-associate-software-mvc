/**
 * Materia: Software Architecture
 * UAGRM - FICCT
 * @author: Ruddy Bryan Quispe Mamani 
 * @version: 1.0.0
 * @since: 29-10-2021
 */

/**
 * Libraries Import
 */
import { QueryResult } from "pg";
import { Connection } from "../database/connection";
import { AsistenciaSocioModel } from "./asistencia_socio.model";

/**
 * @class Attendance by Model Package contained MVC architecture
 */
export class AsistenciaModel {

    /**
     * Attributes
     */
    private codigo: number;
    private fechaRealizado: Date;
    private descripcion: string;
    private listaSocios: Array<AsistenciaSocioModel>;
    private connectionSQL: Connection;

    /**
     * @constructor
     */
    constructor() {
        this.codigo = -1;
        this.fechaRealizado = new Date();
        this.descripcion = "";
        this.connectionSQL = Connection.getInstance();
        this.listaSocios = new Array<AsistenciaSocioModel>();
    }

    public setCodigo = (codigo: number): void => { this.codigo = codigo; }
    public setFechaRealizado = (fechaRealizado: Date): void => { this.fechaRealizado = fechaRealizado; }
    public setDescripcion = (descripcion: string): void => { this.descripcion = descripcion; }
    public setListaSocios = (listaSocios: Array<string>, codAsistencia: number): void => {
        this.listaSocios = new Array<AsistenciaSocioModel>();
        for (let index = 0; index < listaSocios.length; index++) {
            let ciSocio: string = listaSocios[index];
            this.listaSocios.push(new AsistenciaSocioModel());
            this.listaSocios[index].setCiSocio(parseInt(ciSocio));
            this.listaSocios[index].setCodAsistencia(codAsistencia);
        }
    }

    public getCodigo = (): number => this.codigo;
    public getFechaRealizado = (): Date => this.fechaRealizado;
    public getDescripcion = (): string => this.descripcion;
    public getListaSocios = (): Array<AsistenciaSocioModel> => this.listaSocios;

    /**
     * create a new Attendance
     * @returns true if saved successfully
     */
    public create = async (): Promise<boolean> => {
        try {
            let result = await this.connectionSQL.executeSQL(
                `insert into asistencia(descripcion, fecha_realizado) values ('${this.descripcion}','${this.fechaRealizado}') returning codigo;`
            );
            if (!result) {
                return false;
            }
            for await (let associate of this.listaSocios) {
                associate.setCodAsistencia(result.rows[0].codigo);
                let createdAttendanceAssociate: boolean = await associate.create();
                if (!createdAttendanceAssociate) {
                    console.error("\x1b[41m", "\x1b[37m", `Error into AsistenciaModel -> create() -> associate.create()`, "\x1b[0m");
                    return false;
                }
            }
            return true;
        } catch (error) {
            console.error("\x1b[41m", "\x1b[37m", `Error into AsistenciaModel -> create()`, "\x1b[0m");
            return false;
        }
    }

    /**
     * find by codigo Attendance saved
     * @returns object Attendance or undefined if have an error or not find
     */
    public findById = async (): Promise<{ codigo: number, descripcion: string, fecha_realizado: string } | undefined> => {
        try {
            let AttendanceObtained: QueryResult =
                await this.connectionSQL.executeSQL(
                    `select codigo,descripcion,to_char(fecha_realizado ::DATE,'yyyy-mm-dd') as fecha_realizado,descripcion from asistencia where codigo=${this.codigo};`
                );
            return AttendanceObtained.rows[0];
        } catch (error) {
            console.error("\x1b[41m", "\x1b[37m", `Error into AsistenciaModel -> findById()`, "\x1b[0m");
            return undefined;
        }
    }

    public findAsistenciaList = async (): Promise<Array<{ ci_socio: number, nombre: string, cod_asistencia: number, fecha_realizado: string }>> => {
        try {
            let attendanceList = await this.connectionSQL.executeSQL(
                `select as2.ci_socio, s.nombre, as2.cod_asistencia, a.fecha_realizado 
                from asistencia_socio as2, asistencia a, socio s 
                where as2.ci_socio=s.ci and as2.cod_asistencia=a.codigo and as2.cod_asistencia=${this.codigo} order by s.nombre;`
            );
            return attendanceList.rows;
        } catch (error) {
            console.error("\x1b[41m", "\x1b[37m", `Error into AsistenciaModel -> findAsistenciaList()`, "\x1b[0m");
            return [];
        }
    }

    /**
     * update fechaMake of a Attendance with codigo assigned
     * @returns true if updated else return false
     */
    public update = async (): Promise<boolean> => {
        try {
            await this.connectionSQL.executeSQL(
                `update asistencia set fecha_realizado='${this.fechaRealizado}', descripcion='${this.descripcion}' where codigo=${this.codigo};`
            );
            let clean = await this.listaSocios[0].deleteAsistenciaDetail();
            if (clean) {
                for await (let associate of this.listaSocios) {
                    let createdAssociateAttendance = await associate.create();
                    if (!createdAssociateAttendance) {
                        console.error("\x1b[41m", "\x1b[37m", `Error into AsistenciaModel -> delete() -> associate.create()`, "\x1b[0m");
                        return false;
                    }
                }
            } else {
                return false;
            }
            return true;
        } catch (error) {
            console.error("\x1b[41m", "\x1b[37m", `Error into AsistenciaModel -> update()`, "\x1b[0m");
            return false;
        }
    }

    /**
     * remove an Attendance of codigo assigned AsistenciaModel.codigo
     * @returns true if Attendance was removed, return false if has a trouble
     */
    public delete = async (): Promise<boolean> => {
        try {
            for await (let associate of this.listaSocios) {
                let deletedAssociateAttendance = await associate.delete();
                if (!deletedAssociateAttendance) {
                    console.error("\x1b[41m", "\x1b[37m", `Error into AsistenciaModel -> delete() -> associate.delete()`, "\x1b[0m");
                    return false;
                }
            }
            await this.connectionSQL.executeSQL(
                `delete from asistencia where codigo=${this.codigo};`
            );
            return true;
        } catch (error) {
            console.error("\x1b[41m", "\x1b[37m", `Error into AsistenciaModel -> delete()`, "\x1b[0m");
            return false;
        }
    }

    /**
     * get list associates options
     * @returns list associates into attendance
     */
    public findSociosAsistencia = async (): Promise<Array<
        {
            ci: number, nombre: string, telefono: string, direccion: string,
            fecha_afiliacion: string, estado: boolean, nro_puesto: number, verify_asistencia: boolean
        }>> => {
        try {
            let asistenciaList = await this.connectionSQL.executeSQL(
                `select ci, nombre, verify_asistencia(${this.codigo}, ci) from socio;`
            );
            return asistenciaList.rows;
        } catch (error) {
            console.error("\x1b[41m", "\x1b[37m", `Error into AsistenciaModel -> findSociosAsistencia()`, "\x1b[0m");
            console.log(error);
            return [];
        }
    }

    /**
     * get list Attendance saved
     * @returns Array data Attendance
     */
    public findAll = async (): Promise<Array<{ codigo: number, descripcion: string, fecha_realizado: string }>> => {
        try {
            let AttendanceList = await this.connectionSQL.executeSQL(
                `select codigo, descripcion, to_char(fecha_realizado ::DATE,'Day dd Mon yyyy') as fecha_realizado 
                from asistencia 
                order by to_char(fecha_realizado ::DATE,'mm-dd-yyyy') desc;`
            );
            return AttendanceList.rows;
        } catch (error) {
            console.error("\x1b[41m", "\x1b[37m", `Error into AsistenciaModel -> findAll()`, "\x1b[0m");
            return [];
        }
    }
}