/**
 * Materia: Arquitectura de Software
 * UAGRM - FICCT
 * @author: Ruddy Bryan Quispe Mamani 
 * @version: 0.0.1
 * @since: 14-04-2021
 */

import { PagoAporteModel } from '../../model/pago_aporte.model';
import { Response } from '../../options';


export class PagoAporteView {

    /**
     * Attributes
     */
    private nro: number;
    private montoTotal: string;
    private fechaPago: Date;
    private pagoAporteModel: PagoAporteModel;

    /**
     * Constructor
     */
    constructor() {
        this.nro = -1;
        this.montoTotal = "-1";
        this.fechaPago = new Date();
        this.pagoAporteModel = new PagoAporteModel();
    }

    public setNro = (nro: number): void => { this.nro = nro; }
    public setMontoTotal = (montoTotal: string): void => { this.montoTotal = montoTotal; }
    public setFechaPago = (fechaPago: Date): void => { this.fechaPago = fechaPago; }

    public getNro = (): number => this.nro;
    public getMontoTotal = (): string => this.montoTotal;
    public getFechaPago = (): Date => this.fechaPago;

    /**
     * update view Attendance Manage
     * @param response : response HTTP
     */
    public async getPagoAporteManageView(response: Response, listSocio: Array<any>, listAporte: Array<any>): Promise<void> {
        let listPagoAporte = await this.pagoAporteModel.findAll();
        response.render('pago_aporte_manage/pago_aporte_manage', {
            list_aporte: listAporte,
            list_socio: listSocio,
            list_pago_aporte: listPagoAporte
        });
    }

    /**
     * render view contribution manage
     * @param response response http
     */
    public async redirectPagoAporteManageView(response: Response): Promise<void> {
        response.redirect('/pago_aporte_manage');
    }

    /**
     * get List Asistencia Detail Associate
     * @param response : request HTTP
     * @param listAsistencia : response HTTP
     */
    // public async getListAsistencia(response: Response, listAsistencia: Array<any>): Promise<void> {
    //     response.json({ list_asistencia: listAsistencia });
    // }

    /**
     * get edit contribution edit
     * @param response : request HTTP
     * @param nroAsistencia : code contribution selected
     */
    public async getEditPagoAporteView(
        response: Response, nroPago: number,
        listSocio: Array<{ ci: number, nombre: string, telefono: string, direccion: string, fecha_afiliacion: string, estado: boolean, nro_puesto: number }>,
        listAporte: Array<{ codigo: string, descripcion: string, fecha_creacion: string, monto: number }>,
        listDetailPagoAporte: Array<{ codigo: string, descripcion: string, monto: number, nro_pago: number }> | undefined): Promise<void> {

        this.pagoAporteModel.setNro(nroPago);
        let pagoAporteData: { nro: number, montoTotal: string, fecha_realizado: string } | undefined
            = await this.pagoAporteModel.findById();
        if (pagoAporteData) {
            // if exists contribution info
            response.status(200).render('pago_aporte_manage/pago_aporte_edit', {
                pago_aporte_data: pagoAporteData,
                list_socio: listSocio,
                list_aporte: listAporte,
                list_detail_pago_aporte: listDetailPagoAporte ? listDetailPagoAporte : []
            });
        } else {
            // if don't exists contribution
            this.getMessageErrorView(response, "No se encontraron los datos de pago aporte");
        }
    }

    /**
     * Render view error
     * @param response : HTTP response
     * @param message : message error to send
     */
    public async getMessageErrorView(response: Response, message: string): Promise<void> {
        response.render('error', { message_error: message });
    }
}