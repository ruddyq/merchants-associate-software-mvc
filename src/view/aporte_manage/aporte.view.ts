/**
 * Materia: Arquitectura de Software
 * UAGRM - FICCT
 * @author: Ruddy Bryan Quispe Mamani 
 * @version: 0.0.1
 * @since: 14-04-2021
 */
import { AporteModel } from '../../model/aporte.model';
import { Response } from '../../options';


export class AporteView {

    /**
     * Attributes
     */
    private codigo: string;
    private descripcion: string;
    private fecha: Date;
    private monto: number;
    private aporteModel: AporteModel;

    /**
     * Constructor
     */
    constructor() {
        this.aporteModel = new AporteModel();
    }

    public setCodigo = (codigo: string): void => { this.codigo = codigo; }
    public setDescripcion = (descripcion: string): void => { this.descripcion = descripcion; }
    public setFecha = (fecha: Date): void => { this.fecha = fecha; }
    public setMonto = (monto: number): void => { this.monto = monto; }

    public getCodigo = (): string => this.codigo;
    public getDescripcion = (): string => this.descripcion;
    public getFecha = (): Date => this.fecha;
    public getMonto = (): number => this.monto;

    /**
     * update view Contribution Manage
     * @param response : response HTTP
     */
    public async getAporteManageView(response: Response): Promise<void> {
        let contributionList: Array<{ codigo: string, descripcion: string, fecha_creacion: string, monto: number }> = await this.aporteModel.findAll();
        response.render('aporte_manage/aporte_manage', {
            contribution_list: contributionList
        });
    }

    /**
     * render view contribution manage
     * @param response response http
     */
    public async redirectAporteManageView(response: Response): Promise<void> {
        response.redirect('/aporte_manage');
    }

    /**
     * get edit contribution edit
     * @param response : request HTTP
     * @param codeContribution : code contribution selected
     */
    public async getEditAporteView(response: Response, codeContribution: string): Promise<void> {
        this.aporteModel.setCodigo(codeContribution);
        let contributionData: { codigo: string, descripcion: string, fecha_creacion: string, monto: number } | undefined =
            await this.aporteModel.findById();
        if (contributionData) {
            // if exists contribution info
            response.status(200).render('aporte_manage/aporte_edit', contributionData);
        } else {
            // if don't exists contribution
            this.getMessageErrorView(response, "No se encontro los datos");
        }
    }

    /**
     * Render view error
     * @param response : HTTP response
     * @param message : message error to send
     */
    public async getMessageErrorView(response: Response, message: string): Promise<void> {
        response.render('error', { message_error: message });
    }
}