/**
 * Materia: Software Architecture
 * UAGRM - FICCT
 * @author: Ruddy Bryan Quispe Mamani 
 * @version: 1.0.0
 * @since: 09-10-2021
 */

/**
 * Libraries Import
 */
import { QueryResult } from "pg";
import { Connection } from "../database/connection";

/**
 * @class Shop by Model package contained MVC architecture
 */
export class PuestoModel {

    /**
     * Attributes
     */
    private nroPuesto: number;
    private estado: boolean;            // true: habilitado * false: inhabilitado
    private idSector: number;
    private connectionSQL: Connection;

    /**
     * @constructor
     */
    constructor() {
        this.nroPuesto = -1;
        this.estado = false;
        this.idSector = -1;
        this.connectionSQL = Connection.getInstance();
    }

    public setNroPuesto = (nroPuesto: number): void => { this.nroPuesto = nroPuesto; }
    public setEstado = (estado: boolean): void => { this.estado = estado; }
    public setIdSector = (idSector: number): void => { this.idSector = idSector; }

    public getNroPuesto = (): number => this.nroPuesto;
    public getEstado = (): boolean => this.estado;
    public getIdSector = (): number => this.idSector;

    /**
     * create a new shop
     * @returns true if saved successfully, else return false
     */
    public create = async (): Promise<boolean> => {
        try {
            await this.connectionSQL.executeSQL(`insert into puesto(nro_puesto, estado, id_sector) 
                values (${this.nroPuesto}, ${this.estado}, ${this.idSector});`);
            return true;
        } catch (error) {
            console.log("\x1b[41m", "\x1b[37m", `Error into PuestoModel -> create()`, "\x1b[0m");
            return false;
        }
    }

    /**
     * find by id shop saved
     * @returns object shop or undefined if have an error or not find
     */
    public findById = async (): Promise<{ nro_puesto: number, estado: boolean, id_sector: number, descripcion: string } | undefined> => {
        try {
            let shopObtained: QueryResult = await this.connectionSQL.executeSQL(
                `select p.nro_puesto, p.estado, p.id_sector, s.descripcion 
                from puesto p, sector s 
                where p.id_sector=s.id and p.nro_puesto=${this.nroPuesto};`
            );
            return shopObtained.rows[0];
        } catch (error) {
            console.log("\x1b[41m", "\x1b[37m", `Error into PuestoModel -> findById()`, "\x1b[0m");
            return undefined;
        }
    }

    /**
     * update estado and sector assigned by shop No
     * @returns true if updated else return false
     */
    public update = async (): Promise<boolean> => {
        try {
            await this.connectionSQL.executeSQL(
                `update puesto 
                set estado=${this.estado}, id_sector=${this.idSector} 
                where nro_puesto=${this.nroPuesto};`
            );
            return true;
        } catch (error) {
            console.log("\x1b[41m", "\x1b[37m", `Error into PuestoModel -> update()`, "\x1b[0m");
            return false;
        }
    }

    /**
     * remove an shop of shop No assigned
     * @returns true if sector was removed, return false if has a trouble
     */
    public delete = async (): Promise<boolean> => {
        try {
            await this.connectionSQL.executeSQL(
                `delete from puesto where nro_puesto=${this.nroPuesto};`
            );
            return true;
        } catch (error) {
            console.log("\x1b[41m", "\x1b[37m", `Error into PuestoModel -> delete()`, "\x1b[0m");
            return false;
        }
    }

    /**
     * get list shop saved
     * @returns Array data sector
     */
    public findAll = async (): Promise<Array<{ nro_puesto: number, estado: boolean, id_sector: number, descripcion: string }>> => {
        try {
            let shopList = await this.connectionSQL.executeSQL(
                `select p.nro_puesto, p.estado, p.id_sector, s.descripcion 
                from puesto p, sector s 
                where p.id_sector=s.id order by nro_puesto;`);
            return shopList.rows;
        } catch (error) {
            console.log("\x1b[41m", "\x1b[37m", `Error into PuestoModel -> findAll()`, "\x1b[0m");
            return [];
        }
    }

    /**
     * enable or disable one shop
     * @returns true if change its estatus
     */
    public updatePermitOfShop = async (): Promise<boolean> => {
        try {
            await this.connectionSQL.executeSQL(
                `update puesto 
                set estado=not estado 
                where nro_puesto=${this.nroPuesto};`
            );
            return true;
        } catch (error) {
            console.log("\x1b[41m", "\x1b[37m", `Error into PuestoModel -> updatePermitOfShop()`, "\x1b[0m");
            return false;
        }
    }
}