/**
 * Materia: Arquitectura de Software
 * UAGRM - FICCT
 * @author: Ruddy Bryan Quispe Mamani 
 * @version: 0.0.1
 * @since: 27-10-2021
 */

import { PuestoModel } from '../model/puesto.model';
import { SocioModel } from '../model/socio.model';
import { Request, Response, Router } from '../options';
import { SocioView } from '../view/socio_manage/socio.view';

export class SocioController {
    /**
     * Attributes
     */
    public router: Router;
    private socioModel: SocioModel;
    private socioView: SocioView;
    private puestoModel: PuestoModel;

    /**
     * Constructor
     */
    constructor() {
        this.router = Router();
        this.createRoutes();
        this.socioModel = new SocioModel();
        this.socioView = new SocioView();
        this.puestoModel = new PuestoModel();
    }

    /**
     * return list associate with views and options post
     * @param request : request de HTTP
     * @param response : response de HTTP
     */
    public async getViewAssociateManage(request: Request, response: Response): Promise<void> {
        let puestoList = await this.puestoModel.findAll();
        await this.socioView.getAssociateManageView(response, puestoList);
    }

    /**
     * create new associate
     * @param request : request HTTP
     * @param response : response HTTP
     */
    public async createAssociate(request: Request, response: Response): Promise<void> {
        console.log(request.body);
        let { ci, nombre, telefono, direccion, fecha_afiliacion, nro_puesto } = request.body;
        let status = request.body.estado ? true : false;
        this.socioModel.setCI(ci);
        this.socioModel.setNombre(nombre);
        this.socioModel.setTelefono(telefono);
        this.socioModel.setDireccion(direccion);
        this.socioModel.setFechaAfiliacion(fecha_afiliacion);
        this.socioModel.setNroPuesto(nro_puesto != "-1" ? parseInt(nro_puesto) : undefined);
        this.socioModel.setEstado(status);
        let created: boolean = await this.socioModel.create();
        if (created) {
            await this.socioView.redirectAssociateManageView(response);
        } else {
            await this.socioView.getMessageErrorView(response, "No se pudo guardar al nuevo socio");
        }
    }

    /**
     * get a associate specified
     * @param request : request HTTP
     * @param response : response HTTP
     */
    public async getViewEditAssociate(request: Request, response: Response): Promise<void> {
        let { ci_socio } = request.params;
        let puestoList: Array<{ nro_puesto: number, estado: boolean, id_sector: number, descripcion: string }>
            = await this.puestoModel.findAll();
        await this.socioView.getEditAssociateView(response, parseInt(ci_socio), puestoList);
    }

    /**
     * update a associate
     * @param request : request HTTP
     * @param response : response HTTP
     */
    public async updateAssociate(request: Request, response: Response): Promise<void> {
        let { ci_socio } = request.params;
        let { nombre, telefono, direccion, fecha_afiliacion, nro_puesto } = request.body;
        let status = request.body.estado ? true : false;
        this.socioModel.setCI(parseInt(ci_socio));
        this.socioModel.setNombre(nombre);
        this.socioModel.setTelefono(telefono);
        this.socioModel.setDireccion(direccion);
        this.socioModel.setFechaAfiliacion(fecha_afiliacion);
        this.socioModel.setNroPuesto(nro_puesto != "-1" ? parseInt(nro_puesto) : undefined);
        this.socioModel.setEstado(status);
        let updated: boolean = await this.socioModel.update();
        if (updated) {
            // update shop successfully
            this.socioView.redirectAssociateManageView(response);
        } else {
            // it has trouble to update shop
            this.socioView.getMessageErrorView(response, "No se pudo modificar los datos del socio " + ci_socio);
        }
    }

    /**
     * remove one associate
     * @param request : request HTTP
     * @param response : response HTTP
     */
    public async deleteAssociate(request: Request, response: Response): Promise<void> {
        let { ci_socio } = request.params;
        this.socioModel.setCI(parseInt(ci_socio));
        let deleted: boolean = await this.socioModel.delete();
        if (deleted) {
            let puestoList: Array<{ nro_puesto: number, estado: boolean, id_sector: number, descripcion: string }>
                = await this.puestoModel.findAll();
            await this.socioView.getAssociateManageView(response, puestoList);
        } else {
            this.socioView.getMessageErrorView(response, "No se pudo eliminar al socio con CI: " + ci_socio);
        }
    }

    /**
     * change status of a associate
     * @param request : HTTP Request
     * @param response : HTTP Response
     */
    public async permitAssociate(request: Request, response: Response): Promise<void> {
        let { ci_socio } = request.params;
        this.socioModel.setCI(parseInt(ci_socio));
        let updatedPermitAssociate = await this.socioModel.updatePermitOfAssociate();
        if (updatedPermitAssociate) {
            let puestoList: Array<{ nro_puesto: number, estado: boolean, id_sector: number, descripcion: string }>
                = await this.puestoModel.findAll();
            await this.socioView.getAssociateManageView(response, puestoList);
        } else {
            this.socioView.getMessageErrorView(response,
                "No se pudo cambiar el estado del socio con CI: " + ci_socio);
        }
    }

    /**
     * metodo privado para cargar las rutas que disponen en los metodos HTTP
     */
    private createRoutes(): void {
        this.router.route('/').get(async (req: Request, res: Response) => this.getViewAssociateManage(req, res));
        this.router.route('/').post(async (req: Request, res: Response) => this.createAssociate(req, res));
        this.router.route('/:ci_socio').get(async (req: Request, res: Response) => this.getViewEditAssociate(req, res));
        this.router.route('/update/:ci_socio').put(async (req: Request, res: Response) => this.updateAssociate(req, res));
        this.router.route('/delete/:ci_socio').delete(async (req: Request, res: Response) => this.deleteAssociate(req, res));
        this.router.route('/permit/:ci_socio').put(async (req: Request, res: Response) => this.permitAssociate(req, res));
    }
}